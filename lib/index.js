"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Scrollama", {
  enumerable: true,
  get: function get() {
    return _Scrollama.default;
  }
});
Object.defineProperty(exports, "Step", {
  enumerable: true,
  get: function get() {
    return _Step.default;
  }
});

var _Scrollama = _interopRequireDefault(require("./Scrollama"));

var _Step = _interopRequireDefault(require("./Step"));
"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _reactJss = _interopRequireDefault(require("react-jss"));

var _uuid = _interopRequireDefault(require("uuid"));

var _DebugOffset = _interopRequireDefault(require("./DebugOffset"));

var styles = {};
var ZERO_MOE = 1; // zero with some rounding margin of error

var getPageHeight = function getPageHeight() {
  var body = document.body;
  var html = document.documentElement;
  return Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
};

var Scrollama =
/*#__PURE__*/
function (_PureComponent) {
  (0, _inherits2.default)(Scrollama, _PureComponent);

  function Scrollama(props) {
    var _this;

    (0, _classCallCheck2.default)(this, Scrollama);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(Scrollama).call(this, props));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getRefComponent", function (id) {
      var comp = _this[id];
      return comp && comp.current || console.error('Could not retrieve step with id', id);
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getDOMNode", function (step) {
      return step.domNode.current;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "handleResize", function () {
      var _this$state = _this.state,
          stepElIds = _this$state.stepElIds,
          offsetVal = _this$state.offsetVal,
          isEnabled = _this$state.isEnabled;
      var vh = window.innerHeight;

      _this.setState({
        vh: vh,
        ph: getPageHeight(),
        offsetMargin: offsetVal * vh
      });

      stepElIds.forEach(function (id) {
        var step = _this.getRefComponent(id);

        step.updateOffsetHeight();
      });

      if (isEnabled) {
        _this.updateIO();
      }
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "handleEnable", function (enable) {
      var _this$state2 = _this.state,
          isEnabled = _this$state2.isEnabled,
          io = _this$state2.io;

      if (enable && !isEnabled) {
        _this.updateIO();

        _this.setState({
          isEnabled: true
        });
      } else if (!enable) {
        if (io.stepAbove) io.stepAbove.forEach(function (obs) {
          return obs.disconnect();
        });
        if (io.stepBelow) io.stepBelow.forEach(function (obs) {
          return obs.disconnect();
        });

        _this.setState({
          isEnabled: false
        });
      }
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateIO", function () {
      _this.updateStepAboveIO();

      _this.updateStepBelowIO();
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateStepAboveIO", function () {
      var _this$state3 = _this.state,
          io = _this$state3.io,
          stepElIds = _this$state3.stepElIds,
          vh = _this$state3.vh,
          offsetMargin = _this$state3.offsetMargin;

      if (io.stepAbove) {
        io.stepAbove.forEach(function (obs) {
          return obs.disconnect();
        });
      }

      _this.setState({
        io: (0, _objectSpread2.default)({}, io, {
          stepAbove: stepElIds.map(function (id) {
            var step = _this.getRefComponent(id);

            var marginTop = step.state.offsetHeight;
            var marginBottom = -vh + offsetMargin;
            var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
            var options = {
              root: null,
              rootMargin: rootMargin,
              threshold: 0
            };
            var obs = new IntersectionObserver(_this.intersectStepAbove, options);
            obs.observe(step.getDOMNode());
            return obs;
          })
        })
      });
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateStepBelowIO", function () {
      var _this$state4 = _this.state,
          io = _this$state4.io,
          stepElIds = _this$state4.stepElIds,
          vh = _this$state4.vh,
          ph = _this$state4.ph,
          offsetMargin = _this$state4.offsetMargin;

      if (io.stepBelow) {
        io.stepBelow.forEach(function (obs) {
          return obs.disconnect();
        });
      }

      _this.setState({
        io: (0, _objectSpread2.default)({}, io, {
          stepBelow: stepElIds.map(function (id) {
            var step = _this.getRefComponent(id);

            var marginTop = -offsetMargin;
            var marginBottom = ph - vh + step.state.offsetHeight + offsetMargin;
            var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
            var options = {
              root: null,
              rootMargin: rootMargin,
              threshold: 0
            };
            var obs = new IntersectionObserver(_this.intersectStepBelow, options);
            obs.observe(step.getDOMNode());
            return obs;
          })
        })
      });
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateDirection", function () {
      var previousYOffset = _this.state.previousYOffset;
      var _window = window,
          pageYOffset = _window.pageYOffset;

      if (pageYOffset > previousYOffset) {
        _this.setState({
          direction: 'down'
        });
      } else if (pageYOffset < previousYOffset) {
        _this.setState({
          direction: 'up'
        });
      }

      _this.setState({
        previousYOffset: pageYOffset
      });
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "notifyStepEnter", function (step, direction) {
      var stepEnter = _this.state.callback.stepEnter;
      step.enter(direction);
      var resp = {
        element: step.getDOMNode(),
        data: step.getData(),
        direction: direction
      };

      if (stepEnter && typeof stepEnter === 'function') {
        stepEnter(resp);
      }
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "notifyStepExit", function (step, direction) {
      var stepExit = _this.state.callback.stepExit;
      step.exit(direction);
      var resp = {
        element: step.getDOMNode(),
        data: step.getData(),
        direction: direction
      };

      if (stepExit && typeof stepExit === 'function') {
        stepExit(resp);
      }
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "intersectStepAbove", function (entries) {
      _this.updateDirection();

      var _this$state5 = _this.state,
          stepElIds = _this$state5.stepElIds,
          offsetMargin = _this$state5.offsetMargin,
          direction = _this$state5.direction;
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            boundingClientRect = entry.boundingClientRect,
            id = entry.target.id;
        if (!stepElIds.includes(id)) return; // bottom is how far bottom edge of el is from top of viewport

        var bottom = boundingClientRect.bottom,
            height = boundingClientRect.height;
        var bottomAdjusted = bottom - offsetMargin;

        var step = _this.getRefComponent(id);

        if (!step) {
          return;
        }

        var state = step.state.state;

        if (bottomAdjusted >= -ZERO_MOE) {
          if (isIntersecting && direction === 'down' && state !== 'enter') {
            _this.notifyStepEnter(step, direction);
          } else if (!isIntersecting && direction === 'up' && state === 'enter') {
            _this.notifyStepExit(step, direction);
          } else if (!isIntersecting && bottomAdjusted >= height && direction === 'down' && state === 'enter') {
            _this.notifyStepExit(step, direction);
          }
        }
      });
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "intersectStepBelow", function (entries) {
      _this.updateDirection();

      var _this$state6 = _this.state,
          offsetMargin = _this$state6.offsetMargin,
          direction = _this$state6.direction;
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            boundingClientRect = entry.boundingClientRect,
            id = entry.target.id;
        var bottom = boundingClientRect.bottom,
            height = boundingClientRect.height;
        var bottomAdjusted = bottom - offsetMargin;

        var step = _this.getRefComponent(id);

        if (!step) {
          return;
        }

        var state = step.state.state;

        if (bottomAdjusted >= -ZERO_MOE && bottomAdjusted < height && isIntersecting && direction === 'up' && state !== 'enter') {
          _this.notifyStepEnter(step, direction);
        } else if (bottomAdjusted <= ZERO_MOE && !isIntersecting && direction === 'down' && state === 'enter') {
          _this.notifyStepExit(step, direction);
        }
      });
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "addStep", function (id) {
      var stepElIds = _this.state.stepElIds;
      stepElIds.push(id);

      _this.setState({
        stepElIds: stepElIds
      });

      _this.updateIO(); // update observers

    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "removeStep", function (badId) {
      var stepElIds = _this.state.stepElIds; // remove badId from our list of step id's

      var badIndex = stepElIds.findIndex(function (id) {
        return id === badId;
      });

      if (badIndex >= 0) {
        stepElIds.splice(badIndex, badIndex + 1);

        _this.setState({
          stepElIds: stepElIds
        });
      }

      delete _this[badId]; // remove our ref to the removed step

      _this.updateIO(); // update observers

    });
    var _this$props = _this.props,
        offset = _this$props.offset,
        debug = _this$props.debug,
        children = _this$props.children,
        onStepEnter = _this$props.onStepEnter,
        onStepExit = _this$props.onStepExit;
    var _stepElIds = [];

    _react.default.Children.forEach(children, function () {
      var childId = _uuid.default.v4();

      _this[childId] = _react.default.createRef();

      _stepElIds.push(childId);
    });

    var _offsetVal = 0;

    if (offset && !isNaN(offset)) {
      _offsetVal = Math.min(Math.max(0, offset), 1);
    }

    _this.state = {
      isEnabled: false,
      debugMode: debug,
      callback: {
        stepEnter: onStepEnter,
        stepExit: onStepExit
      },
      io: {},
      stepElIds: _stepElIds,
      direction: null,
      vh: 0,
      ph: 0,
      offsetVal: _offsetVal,
      offsetMargin: 0,
      previousYOffset: -1
    };

    if (typeof window !== "undefined") {
      window.addEventListener('resize', _this.handleResize);
    }

    return _this;
  }

  (0, _createClass2.default)(Scrollama, [{
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                require('intersection-observer');

                _context.next = 3;
                return this.handleResize();

              case 3:
                this.handleEnable(true);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.handleResize);
      this.handleEnable(false);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state7 = this.state,
          stepElIds = _this$state7.stepElIds,
          debugMode = _this$state7.debugMode,
          offsetMargin = _this$state7.offsetMargin,
          offsetVal = _this$state7.offsetVal;
      var _this$props2 = this.props,
          children = _this$props2.children,
          rest = (0, _objectWithoutProperties2.default)(_this$props2, ["children"]);
      return _react.default.createElement("div", rest, debugMode && _react.default.createElement(_DebugOffset.default, {
        offsetMargin: offsetMargin,
        offsetVal: offsetVal
      }), _react.default.Children.map(children, function (child, index) {
        var isNew = !stepElIds[index];

        if (isNew) {
          var id = _uuid.default.v4();

          _this2[id] = _react.default.createRef();
        } else {
          var id = stepElIds[index];
        }

        return _react.default.cloneElement(child, {
          id: id,
          isNew: isNew,
          addSelf: function addSelf() {
            return _this2.addStep(id);
          },
          removeSelf: function removeSelf() {
            return _this2.removeStep(id);
          },
          ref: _this2[id]
        });
      }));
    }
  }]);
  return Scrollama;
}(_react.PureComponent);

Scrollama.defaultProps = {
  offset: 0.5,
  debug: false
};

var _default = (0, _reactJss.default)(styles)(Scrollama);

exports.default = _default;
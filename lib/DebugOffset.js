"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactJss = _interopRequireDefault(require("react-jss"));

var styles = {
  marker: {
    position: 'fixed',
    left: 0,
    width: '100%',
    height: 0,
    borderTop: '2px dashed black',
    zIndex: 9999
  },
  offsetInfo: {
    fontSize: '12px',
    fontFamily: 'monospace',
    margin: 0,
    padding: 6
  }
};

var DebugOffset = function DebugOffset(_ref) {
  var classes = _ref.classes,
      offsetMargin = _ref.offsetMargin,
      offsetVal = _ref.offsetVal;
  return _react.default.createElement("div", {
    className: classes.marker,
    style: {
      top: offsetMargin
    }
  }, _react.default.createElement("p", {
    className: classes.offsetInfo
  }, "trigger: ", offsetVal));
};

var _default = (0, _reactJss.default)(styles)(DebugOffset);

exports.default = _default;
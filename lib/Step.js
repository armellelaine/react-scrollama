"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var Step =
/*#__PURE__*/
function (_PureComponent) {
  (0, _inherits2.default)(Step, _PureComponent);

  function Step() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, Step);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(Step)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "state", {
      direction: null,
      // 'up' or 'down'
      state: null,
      // 'enter' or 'exit'
      offsetHeight: null
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "domNode", _react.default.createRef());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getDOMNode", function () {
      return _this.domNode.current;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getData", function () {
      return _this.props.data;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateOffsetHeight", function () {
      _this.setState({
        offsetHeight: _this.domNode.current.offsetHeight
      });
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "enter", function (direction) {
      return _this.setState({
        state: 'enter',
        direction: direction
      });
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "exit", function (direction) {
      return _this.setState({
        state: 'exit',
        direction: direction
      });
    });
    return _this;
  }

  (0, _createClass2.default)(Step, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var _this$props = this.props,
          isNew = _this$props.isNew,
          addSelf = _this$props.addSelf;

      if (isNew) {
        window.requestAnimationFrame(function () {
          _this2.updateOffsetHeight();

          addSelf();
        });
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.props.removeSelf();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          id = _this$props2.id,
          children = _this$props2.children;
      return _react.default.cloneElement(_react.default.Children.only(children), {
        id: id,
        ref: this.domNode
      });
    }
  }]);
  return Step;
}(_react.PureComponent);

var _default = Step;
exports.default = _default;